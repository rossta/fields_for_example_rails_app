require 'test_helper'

class InquiryItemPricesControllerTest < ActionController::TestCase
  setup do
    @inquiry_item_price = inquiry_item_prices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inquiry_item_prices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inquiry_item_price" do
    assert_difference('InquiryItemPrice.count') do
      post :create, inquiry_item_price: { amount: @inquiry_item_price.amount, inquiry_item_id: @inquiry_item_price.inquiry_item_id }
    end

    assert_redirected_to inquiry_item_price_path(assigns(:inquiry_item_price))
  end

  test "should show inquiry_item_price" do
    get :show, id: @inquiry_item_price
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inquiry_item_price
    assert_response :success
  end

  test "should update inquiry_item_price" do
    patch :update, id: @inquiry_item_price, inquiry_item_price: { amount: @inquiry_item_price.amount, inquiry_item_id: @inquiry_item_price.inquiry_item_id }
    assert_redirected_to inquiry_item_price_path(assigns(:inquiry_item_price))
  end

  test "should destroy inquiry_item_price" do
    assert_difference('InquiryItemPrice.count', -1) do
      delete :destroy, id: @inquiry_item_price
    end

    assert_redirected_to inquiry_item_prices_path
  end
end
