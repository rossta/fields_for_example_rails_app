require 'test_helper'

class InquiryItemsControllerTest < ActionController::TestCase
  setup do
    @inquiry_item = inquiry_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inquiry_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inquiry_item" do
    assert_difference('InquiryItem.count') do
      post :create, inquiry_item: { inquiry_id: @inquiry_item.inquiry_id, name: @inquiry_item.name }
    end

    assert_redirected_to inquiry_item_path(assigns(:inquiry_item))
  end

  test "should show inquiry_item" do
    get :show, id: @inquiry_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inquiry_item
    assert_response :success
  end

  test "should update inquiry_item" do
    patch :update, id: @inquiry_item, inquiry_item: { inquiry_id: @inquiry_item.inquiry_id, name: @inquiry_item.name }
    assert_redirected_to inquiry_item_path(assigns(:inquiry_item))
  end

  test "should destroy inquiry_item" do
    assert_difference('InquiryItem.count', -1) do
      delete :destroy, id: @inquiry_item
    end

    assert_redirected_to inquiry_items_path
  end
end
