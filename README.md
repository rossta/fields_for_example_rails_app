== README

Demonstrates use of doubly nested attributes in a nested `fields_for` form.

![](https://www.evernote.com/shard/s111/sh/bd2b635b-9f35-47b4-a1b6-fc7c72d38bd2/929929c6178f84f14fe81aa9eb2f8fb3/deep/0/Screenshot%207/31/15,%203:43%20PM.jpg)
