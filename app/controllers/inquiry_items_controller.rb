class InquiryItemsController < ApplicationController
  before_action :set_inquiry_item, only: [:show, :edit, :update, :destroy]

  # GET /inquiry_items
  # GET /inquiry_items.json
  def index
    @inquiry_items = InquiryItem.all
  end

  # GET /inquiry_items/1
  # GET /inquiry_items/1.json
  def show
  end

  # GET /inquiry_items/new
  def new
    @inquiry_item = InquiryItem.new
  end

  # GET /inquiry_items/1/edit
  def edit
  end

  # POST /inquiry_items
  # POST /inquiry_items.json
  def create
    @inquiry_item = InquiryItem.new(inquiry_item_params)

    respond_to do |format|
      if @inquiry_item.save
        format.html { redirect_to @inquiry_item, notice: 'Inquiry item was successfully created.' }
        format.json { render :show, status: :created, location: @inquiry_item }
      else
        format.html { render :new }
        format.json { render json: @inquiry_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inquiry_items/1
  # PATCH/PUT /inquiry_items/1.json
  def update
    respond_to do |format|
      if @inquiry_item.update(inquiry_item_params)
        format.html { redirect_to @inquiry_item, notice: 'Inquiry item was successfully updated.' }
        format.json { render :show, status: :ok, location: @inquiry_item }
      else
        format.html { render :edit }
        format.json { render json: @inquiry_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inquiry_items/1
  # DELETE /inquiry_items/1.json
  def destroy
    @inquiry_item.destroy
    respond_to do |format|
      format.html { redirect_to inquiry_items_url, notice: 'Inquiry item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inquiry_item
      @inquiry_item = InquiryItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inquiry_item_params
      params.require(:inquiry_item).permit(:name, :inquiry_id)
    end
end
