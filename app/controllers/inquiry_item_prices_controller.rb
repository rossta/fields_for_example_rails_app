class InquiryItemPricesController < ApplicationController
  before_action :set_inquiry_item_price, only: [:show, :edit, :update, :destroy]

  # GET /inquiry_item_prices
  # GET /inquiry_item_prices.json
  def index
    @inquiry_item_prices = InquiryItemPrice.all
  end

  # GET /inquiry_item_prices/1
  # GET /inquiry_item_prices/1.json
  def show
  end

  # GET /inquiry_item_prices/new
  def new
    @inquiry_item_price = InquiryItemPrice.new
  end

  # GET /inquiry_item_prices/1/edit
  def edit
  end

  # POST /inquiry_item_prices
  # POST /inquiry_item_prices.json
  def create
    @inquiry_item_price = InquiryItemPrice.new(inquiry_item_price_params)

    respond_to do |format|
      if @inquiry_item_price.save
        format.html { redirect_to @inquiry_item_price, notice: 'Inquiry item price was successfully created.' }
        format.json { render :show, status: :created, location: @inquiry_item_price }
      else
        format.html { render :new }
        format.json { render json: @inquiry_item_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inquiry_item_prices/1
  # PATCH/PUT /inquiry_item_prices/1.json
  def update
    respond_to do |format|
      if @inquiry_item_price.update(inquiry_item_price_params)
        format.html { redirect_to @inquiry_item_price, notice: 'Inquiry item price was successfully updated.' }
        format.json { render :show, status: :ok, location: @inquiry_item_price }
      else
        format.html { render :edit }
        format.json { render json: @inquiry_item_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inquiry_item_prices/1
  # DELETE /inquiry_item_prices/1.json
  def destroy
    @inquiry_item_price.destroy
    respond_to do |format|
      format.html { redirect_to inquiry_item_prices_url, notice: 'Inquiry item price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inquiry_item_price
      @inquiry_item_price = InquiryItemPrice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inquiry_item_price_params
      params.require(:inquiry_item_price).permit(:amount, :inquiry_item_id)
    end
end
