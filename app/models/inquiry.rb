class Inquiry < ActiveRecord::Base
  has_many :inquiry_items

  accepts_nested_attributes_for :inquiry_items
end
