class InquiryItem < ActiveRecord::Base
  belongs_to :inquiry
  has_many :inquiry_item_prices

  accepts_nested_attributes_for :inquiry_item_prices
end
