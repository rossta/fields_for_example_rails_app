class CreateInquiryItems < ActiveRecord::Migration
  def change
    create_table :inquiry_items do |t|
      t.string :name
      t.references :inquiry, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
