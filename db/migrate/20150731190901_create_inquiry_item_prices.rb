class CreateInquiryItemPrices < ActiveRecord::Migration
  def change
    create_table :inquiry_item_prices do |t|
      t.integer :price
      t.references :inquiry_item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
