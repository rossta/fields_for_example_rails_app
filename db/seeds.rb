# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

10.times do |i|
  inquiry = Inquiry.create(name: "Inquiry #{i}")

  rand(20).times do |j|
    inquiry.inquiry_items.create(name: "Item #{i}:#{j}")
  end
end
